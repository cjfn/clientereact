import React from 'react';  
import { Row, Form, Col, Button } from 'react-bootstrap';  

class AddEmpleado extends React.Component{
    constructor(props){
        super(props);

        this.initialState = {
            id: '',
            nombre: '',
            apellido: '',
            fecha_inicio: '',
            departamento: '',
            puesto: '',
            salario: ''
        }
        

        if(props.empleado.id){
            //this.state. = props.empleado
            this.state = {                          
              id: props.empleado.id,
              nombre: props.empleado.nombre,
              apellido: props.empleado.apellido,
              fecha_inicio: props.empleado.fecha_inicio,
              departamento: props.empleado.departamento,
              puesto: props.empleado.puesto,
              salario: props.empleado.salario
            }

            console.log('pasate')
        } else {
          this.state = this.initialState;
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        console.log(this.state.id);
    }

    handleChange(event){
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            [name]:value
        })
    }

    handleSubmit(event){
        event.preventDefault();
        this.props.onFormSubmit(this.state);  
        this.setState(this.initialState);  
    }

    render(){
        let pageTitle;
        let actionStatus;
        if(this.state.id){
            pageTitle = <h2>Editar Empleado</h2>
            actionStatus = <b>Modificar</b>
        }
        else{
            pageTitle = <h2>Agregar Empleado</h2>
            actionStatus = <b>Guardar</b>
        }
        return (  
            <div>        
              <h2> {pageTitle}</h2>  
              <Row>  
                <Col sm={7}>  
                  <Form onSubmit={this.handleSubmit}>  
                    <Form.Group controlId="nombre">  
                      <Form.Label>nombre</Form.Label>  
                      <Form.Control  
                        type="text"  
                        name="nombre"  
                        value={this.state.nombre}  
                        onChange={this.handleChange}  
                        placeholder="nombre" />  
                    </Form.Group>  
                    <Form.Group controlId="apellido">  
                      <Form.Label>apellido</Form.Label>  
                      <Form.Control  
                        type="text"  
                        name="apellido"  
                        value={this.state.apellido}  
                        onChange={this.handleChange}  
                        placeholder="apellido" />  
                    </Form.Group>  
                    <Form.Group controlId="fecha_inicio">  
                      <Form.Label>fecha_inicio</Form.Label>  
                      <Form.Control  
                        type="text"  
                        name="fecha_inicio"  
                        value={this.state.fecha_inicio}  
                        onChange={this.handleChange}  
                        placeholder="fecha_inicio" />  
                    </Form.Group>  
                    <Form.Group controlId="departamento">  
                      <Form.Label>departamento</Form.Label>  
                      <Form.Control  
                        type="text"  
                        name="departamento"  
                        value={this.state.departamento}  
                        onChange={this.handleChange}  
                        placeholder="departamento" />  
                    </Form.Group>  
                    <Form.Group controlId="puesto">  
                      <Form.Label>puesto</Form.Label>  
                      <Form.Control  
                        type="text"  
                        name="puesto"  
                        value={this.state.puesto}  
                        onChange={this.handleChange}  
                        placeholder="puesto" />  
                    </Form.Group>  
        
                    <Form.Group controlId="salario">  
                      <Form.Label>Salario</Form.Label>  
                      <Form.Control  
                        type="salario"  
                        name="salario"  
                        value={this.state.salario}  
                        onChange={this.handleChange}  
                        placeholder="salario" />  
                    </Form.Group>  
                    <Form.Group>  
                      <Form.Control type="hidden" name="id" value={this.state.id} />  
                      <Button variant="success" type="submit">{actionStatus}</Button>            
        
                    </Form.Group>  
                  </Form>  
                </Col>  
              </Row>  
            </div>  
          )  

    }
}

export default AddEmpleado;