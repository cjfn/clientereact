import React, {Component} from 'react';

import {Container, Button} from 'react-bootstrap';
import EmpleadoList from './GetEmpleado';
import AddEmpleado from './AddEmpleado';
import axios from 'axios';
const apiUrl = 'http://localhost:4200/test';

class EmpleadoActionApp extends Component {
    constructor(props){
        super(props);
        this.state = {
            isAddEmpleado: false,
            error: null,
            response: {},
            empleadoData: {},
            isEditEmpleado: false,
            isEmpleadoDetails: true,
        }

        this.onFormSubmit = this.onFormSubmit.bind(this);  
    }

    onCreate(){
        this.setState({ isAddEmpleado: true });  
        this.setState({ isEmpleadoDetails: false });  
    }

    onDetails() {  
        this.setState({ isEmpleadoDetails: true });  
        this.setState({ isAddEmpleado: false });  
        this.setState({ isEditEmpleado: true }); 
      }  

      onFormSubmit(data) {  
        //this.setState({ isAddEmpleado: true });  
        //this.setState({ isEmpleadoDetails: false });  
        console.log(this.state.isEditEmpleado);
        if (this.state.isEditEmpleado) {  
         axios.put(apiUrl+ "/" + data.id, { crossdomain: true }, data).then(result => {  
          alert(result.data);  
            this.setState({  
              response:result,    
              isAddEmpleado: false,  
              isEditEmpleado: false  
            })  
          });  
        } else {  
         
         axios.post(apiUrl,data).then(result => {  
          alert(result.data);  
            this.setState({  
              response:result,    
              isAddEmpleado: false,  
              isEditEmpleado: false  
            })  
          });  
        }  
        
      }  

      editEmpleado = id => {
          this.setState({isEmpleadoDetails:false});
          axios.get(apiUrl + "/" + id, { crossdomain: true }).then(result => {  
  
            this.setState({  
              isEditEmpleado: false,  
              isAddEmpleado: true,  
              empleadoData: result.data           
            });  
          },  
          (error) => {  
            this.setState({ error });  
          }  
        )  
      }

      render(){
          let empleadoForm;
          if(this.state.isAddEmpleado  || this.state.isEditEmpleado){
              empleadoForm = <AddEmpleado onFormSubmit={this.onFormSubmit} empleado = {this.state.empleadoData} />
          }
          return (  
            <div className="App">  
              <Container>  
                <h1 style={{ textAlign: 'center' }}>CURD operation in React</h1>  
                <hr></hr>  
                {!this.state.isEmpleadoDetails && <Button variant="primary" onClick={() => this.onDetails()}> Empleado Detalles</Button>}  
                {!this.state.isAddEmpleado && <Button variant="primary" onClick={() => this.onCreate()}>Add Empleado</Button>}  
                <br></br>  
                {!this.state.isAddEmpleado && <EmpleadoList editEmpleado={this.editEmpleado} />}  
                {empleadoForm}  
              </Container>  
            </div>  
          ); 
      }
}

export default EmpleadoActionApp;