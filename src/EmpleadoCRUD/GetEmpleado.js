import React from 'react';
import {Table,Button} from 'react-bootstrap';
import axios from 'axios';

const apiUrl = 'http://localhost:4200';

class EmpleadoList extends React.Component{
    constructor(props){
        super(props);
            this.state = {
                error:null,
                empleados:[],
                response: {}
            }
        }

    componentDidMount(){
        axios.get(apiUrl+'/test', { crossdomain: true }).then(response => response.data).then(
            (result)=>{
                this.setState({
                    empleados:result
                    
                    
                });
            },
            (error)=>{
                this.setState({error});
            } 
        )
    }

    deleteEmpleado(id) {  
        const { empleados } = this.state;     
       axios.delete(apiUrl + '/test/' + id, { crossdomain: true }).then(result=>{  
         alert(result.data);  
          this.setState({  
            response:result,  
            empleados:empleados.filter(empleado=>empleado.id !== id)  
          });  
        });  
      }  

      render(){
          const{error,empleados}=this.state;
          if(error){
              return(
                  <div>Error:{error.message}</div>
              )
          }
          else{
            
           
              return(
                  <div>
                      <Table>
                          <thead className="btn-primary">
                            <tr>
                                <th>nombre</th>
                                <th>apellido</th>
                                <th>departamento</th>
                                <th>puesto</th>
                                <th>salario</th>
                                <th>fecha inicio</th>
                                <th>Opciones</th>
                            </tr>
                          </thead>
                          <tbody>
                              {empleados.map(empleado => (
                                  <tr key={empleado.id}>
                                    <td>{empleado.nombre}</td>
                                    <td>{empleado.apellido}</td>
                                    <td>{empleado.departamento}</td>
                                    <td>{empleado.puesto}</td>
                                    <td>{empleado.salario}</td>
                                    <td>{empleado.fecha_inicio}</td>
                                    <td><Button variant="info" onClick={()=> this.props.editEmpleado(empleado.id)}>Editar</Button></td>
                                    <td><Button variant="danger" onClick={()=> this.deleteEmpleado(empleado.id)}>Eliminar</Button></td>
                                  </tr>
                              ))}
                          </tbody>
                      </Table>
                  </div>
              )
          }
      }
}

export default EmpleadoList;